**Python interface GUI d'un tchat en Local**

---

## Information

Vous disposer de 2 scripts majeur qui sont <u>server.py</u> qui est le serveur et <u>main.py</u> qui est le client

Utilisation du PORT=5000 et pour HOST=127.0.0.1, Assurerez-vous qu'ils sont disponibles
---

## Fonctionnement

2 méthodes pour faire marcher la machine

## 1er

1. Cliquer sur l'executable se trouvant dans ce chemin <u>socket\dist\server.exe</u>, si tous se passent bien, il y aura un print <u>sever is running ...</u> tous les 1 seconde
2. Cliquer sur l'executable se trouvant dans ce chemin <u>socket\dist\main.exe</u>, si tous se passe bien une interface apparaîtra.
3. Cliquer sur l'executable se trouvant dans ce chemin <u>socket\dist\main.exe</u>, si tous se passe bien une interface apparaîtra.

## 2ième

1. Ouvrer 3 terminals pointer sur la racine du projet
2. Entrer dans le 1er terminal : <u>py server.py</u>, si tous se passent bien, il y aura un print <u>sever is running ...</u> tous les 1 seconde
3. Entrer dans le 2ième terminal : <u>py main.py</u>, si tous se passe bien une interface apparaîtra.
4. Entrer dans le 3ième terminal : <u>py main.py</u>, si tous se passe bien une interface apparaîtra.

## Image

1. Lancement du server
![Screenshot](img/server.PNG)
2. Lancement de l'interface GUI (login)
![Screenshot](img/login.PNG)
3. Lancement de l'interface GUI (tchat) ( client numéro 1 )
![Screenshot](img/tchat.PNG)
4. Lancement de l'interface GUI (tchat) ( client numéro 2 )
![Screenshot](img/tchat2.PNG)