import socket, threading, sys, pickle
from app.classApp.dataProgram import dataProgram
import time,os,json

class Server():
    def __init__(self, host: str = '127.0.0.1', port: int = 5000, nickname: str = 'Server', buffer_recv: int = 4096, max_socket_conn: int = 10):
        self.clients = [] # addr socket clients
        self.buffer_recv = buffer_recv
        self.nickname = nickname
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.socket.bind((host, port))
        self.socket.listen(max_socket_conn)
        self.socket.setblocking(False)

        # début de connection de l'utilisateur
        accept_conn_thread = threading.Thread(target=self.accept_connection)
        accept_conn_thread.daemon = True
        accept_conn_thread.start()

        # gestion des données envoyée via les sockets
        process_conn_thread = threading.Thread(target=self.process_connection)
        process_conn_thread.daemon = True
        process_conn_thread.start()

        self.data = dataProgram()
        self.clientsLogged = [] # client (pseudo) connecté sur le tchat

    def accept_connection(self) -> None:
        while True:
            try:
                conn, addr = self.socket.accept()
                conn.setblocking(False)
                self.clients.append(conn)
                print('Un nouvelle utilisateur est connecté')
            except socket.error as err:
                pass

    def process_connection(self) -> None:
        print('\t\t***** Serveur lancé ******\n')
        while True:
            for client in self.clients:
                try:
                    data = self.get_data(client)
                    if data:
                        data = pickle.loads(data)
                        # ici si l'on reçoit une donnée du type MSG_USER_CONN cela veut dire qu'ont a à faire à une personne qui vient
                        # de se connecter
                        if data['type'] == self.data.MSG_USER_CONN:
                            self.clientsLogged.append(data['client'])
                            data['client'] = self.clientsLogged
                        self.send_msg_to_all(client, data)
                except:
                    pass

    def get_data(self, client = None) -> str:
        return client.recv(self.buffer_recv)

    def parse_data(self, data: dict) -> str:
        return pickle.dumps(data)

    def parse_msg(self, data: dict) -> str:
        return pickle.loads(data)

    def send_msg_to_all(self,client = None, data: dict = None) -> None:
        for c in self.clients:
            try:
                print(data)
                c.send(self.parse_data(data))
            except socket.error as err:
                # si erreur de connection on remove le client
                print(err)
                self.clients.remove(c)
                time.sleep(2)

    def close(self):
        self.socket.close()

def main():
     nickname = 'Server'
     server = Server(nickname=nickname)
     while True:
        print('sever is running ...')
        time.sleep(1)

if __name__ == '__main__':
    main()
