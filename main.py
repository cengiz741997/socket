
from app.classApp.interfaceTkLogin import interfaceTkLogin
from app.classApp.interfaceTkTchat import interfaceTkTchat
from app.classApp.dataProgram import dataProgram
from app.help.help import Help

class Program:
    def __init__(self):

        self.__interfaceTkLogin = interfaceTkLogin()
        self.__interfaceTkTchat = ''
        self.__dataProgram = dataProgram()

    def isLogged(self,userPseudo:str) -> None:
        print('Destruction de la page de connection')
        print('Voici le pseudo du client : {}'.format(userPseudo))
        self.__dataProgram.setUserLogged(userPseudo)
        # création d'une nouvelle window tchat
        self.__interfaceTkTchat = interfaceTkTchat(self.__dataProgram)
        self.__interfaceTkTchat.tchatInterface()

    def initialize(self) -> None:

        # passe la méthode d'ajout d'un utilisateur à l'interface
        # toutes les données seront stockées dans self.__data
        self.__interfaceTkLogin.setSetterMethodUserLogged(self.isLogged)
        # création de la page de connexion celle ci est destroy quand la personne se connecte
        self.__interfaceTkLogin.loggedInterface()

if __name__=='__main__':
    root = Program()
    root.initialize()
