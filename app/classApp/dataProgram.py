from tkinter import *
import tkinter as tk

class dataProgram:
    def __init__(self):
        self.__userLogged = ''

        self.MSG_USER_CONN = 5
        self.MSG_USER_MESSAGE = 6

    def setUserLogged(self,user : str) -> None:
        self.__userLogged = user

    def getUserLogged(self) -> str:
        return self.__userLogged
