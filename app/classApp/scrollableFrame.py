import tkinter as tk
from tkinter import ttk

class scrollableFrame(ttk.Frame):
    def __init__(self, container, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        self.scrollableFrame = ''
        self.scrollbar = ''
        self.canvas = ''

    def createCanvas(self,widthCanvas : int, heightCanvas : int ) -> None:

        self.canvas = tk.Canvas(self,width=widthCanvas,height=heightCanvas,bg="white")
        self.scrollbar = ttk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.scrollableFrame = ttk.Frame(self.canvas)

        self.scrollableFrame.bind(
            "<Configure>",
            lambda e: self.canvas.configure(
                scrollregion=self.canvas.bbox("all")
            )
        )

        self.canvas.create_window((0, 0), window=self.scrollableFrame, anchor="nw")
        self.canvas.configure(yscrollcommand=self.scrollbar.set)

        self.canvas.pack(anchor="ne",side="left", expand=True)
        self.scrollbar.pack(side="right", fill="y")

    def getScrollableFrame(self) -> ttk.Frame:
        return self.scrollableFrame

    def getCanvas(self) -> ttk.Frame:
        return self.canvas
