import tkinter as tk
from tkinter import *
from app.help.help import Help
from tkinter import messagebox
import socket, threading, sys, pickle
from app.classApp.client import client
from app.classApp.scrollableFrame import scrollableFrame
from datetime import datetime
from app.classApp.dataProgram import dataProgram

class interfaceTkTchat(tk.Tk):

    def __init__(self,data,*args,**kwargs):
        super().__init__(*args,**kwargs)

        self.input_message_client = tk.StringVar(self)
        self.currentClient = client(nickname=data.getUserLogged())
        self.currentClient.setMethodMessageToInterface(self.getMessage)
        self.data = data
        self.frameMessages = ''
        self.frameClients = ''

    def sendMessage(self) -> None:
        self.currentClient.process_send_message(self.input_message_client.get())
        self.input_message_client.set('')

    def getMessage(self,data: dict) -> None:

        typeAction = data['type']
        # ici on n'envoie pas le message sur l'interface pour que chaque utilisateur puisse voir
        # ce que les clients écrivent
        if typeAction == self.data.MSG_USER_MESSAGE: 
            self.sendMessageToTchat(data)
        # ici c'est pour lister qu'elle utilisateur est connecté sur le tchat et l'afficher sur
        # l'interface
        else:
            self.sendNewUserLogged(data)

    def sendNewUserLogged(self,data:dict) -> None:

        clients = data['client']
        for c in clients:
            clientLabel = Label(
                    self.frameClients.getScrollableFrame(),text="{}".format(c),
                    anchor='w',bg="white",padx=1,pady=1)
            clientLabel.pack(fill='x')  
        
        

    def sendMessageToTchat(self,data:dict) -> None:
        message = data['msg']
        name = data['client']
        name = 'Vous' if self.data.getUserLogged() == name else name
  
        if not message:
            return

        # ici on découpe le message par 50 caractères par ligne
        count =  round(len(message) / 50)

        count = 1 if count == 0 else count

        # on ajoute un label qui sert uniquement despacement pour que le diff messages soit bien espacé entre eux
        spaceLabel = Label(
                    self.frameMessages.getScrollableFrame(),text="",
                    anchor='w',bg="white",padx=2,pady=7)
        spaceLabel.pack(fill='x')

        for i in range(count):
            messageLabel = ''
            # le premier message comporte le nom de la personne ainsi que les 50 premiers caractères
            if i == 0:
                messageFormat = message[0:50]
                today = datetime.now()
                currentDate = today.strftime("%d/%m/%Y à %H:%M")
                messageFormat = "{} [{}] : {}".format(name,currentDate,messageFormat)
                messageLabel = Label(
                    self.frameMessages.getScrollableFrame(),text="{}".format(messageFormat),
                    anchor='w',bg="white",padx=2,pady=2)
            # ensuite on ajoute les autres 50 caractères suivants ainsi desuite
            # ici on ajoute un tiret à la fin pour dire que le message comporte une suite 
            elif i != (count-1):
                messageFormat = message[(50*i):(50*i)+50]
                messageLabel = Label(
                    self.frameMessages.getScrollableFrame(),text="{} -".format(messageFormat),
                    anchor='w',bg="white",padx=1,pady=1)
            # fin de message pas de tiret à la fin
            else:
                messageFormat = message[(50*i):(50*i)+50]
                messageLabel = Label(
                    self.frameMessages.getScrollableFrame(),text="{}".format(messageFormat),
                    anchor='n',bg="white",padx=1,pady=1)

            messageLabel.pack(fill='both')    

        self.frameMessages.getCanvas().update_idletasks()
        self.frameMessages.getCanvas().yview_moveto('2')
            
    def tchatInterface(self) -> None:
        self.title(Help.Title_Program)
        self.geometry('1000x650')
        self.resizable(width=False, height=False)

        label = Label(text="Client(s) connecté(s)" , font="Arial 12 underline")
        label.place(x=20,y=25)
        
        # contient la liste des clients connectés
        self.frameClients = scrollableFrame(self,borderwidth=2, relief="groove")
        self.frameClients.createCanvas(200,450)
        self.frameClients.pack(anchor = "nw", side = "left",pady=50)

        
        label = Label(text="Tchat" , font="Arial 12 underline")
        label.place(x=425,y=25)
        
        # contient tous message envoyé par les clients
        self.frameMessages = scrollableFrame(self,borderwidth=2, relief="groove")
        self.frameMessages.createCanvas(560,450)
        self.frameMessages.pack(anchor = "ne", side = "right",pady=50)

        input_message = Entry(self,textvariable=self.input_message_client,fg="blue",bd=3,selectbackground='violet',font=('Arial'))
        input_message.place(x=1,y=540,height=110,width=780)

        btn_submit = Button(self,text='Envoyer',
                                                 fg='White',
                                                 bg= 'dark green',height = 7, width = 30,command=self.sendMessage)
        btn_submit.place(x=780,y=540)
        self.mainloop()
