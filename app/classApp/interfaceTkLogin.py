import tkinter as tk
from tkinter import *
from app.help.help import Help
from tkinter import messagebox

class interfaceTkLogin(tk.Tk):
        def __init__(self,*args,**kwargs):
            super().__init__(*args,**kwargs)
            self.__methodSetUserLogged = ''
            self.__pseudoStringVar = tk.StringVar(self)

        def connexion(self) -> None:
            pseudo = self.__pseudoStringVar.get()

            if not pseudo:
                tk.messagebox.showwarning('Warning', 'Entrez un pseudo')
                return

            if len(pseudo) > 15:
                tk.messagebox.showwarning('Warning', 'Maximum 15 charactères')
                return
            print('connection réussi')
            self.destroy()
            self.__methodSetUserLogged(self.__pseudoStringVar.get())

        def loggedInterface(self) -> None:

            self.title(Help.Title_Program)
            self.geometry('1000x650')
            self.resizable(width=False, height=False)

            # si l'utilisateur n'a pas rentré de pseudo
            l = Label(self,text=Help.Title_Program,bg=Help.Base_Color,width=110,pady=10,font=("Arial", 12))
            l.grid(column=0,row=0, ipadx=5, pady=5, sticky=W)

            frame = Frame(self,width=400,height=200,bg=Help.Base_Color,pady=150,padx=5)
            frame.place(relx=.5, rely=.5,anchor= CENTER)

            pseudo = Entry(frame,textvariable=self.__pseudoStringVar,width=25,font=('Arial 14'))
            pseudo.pack(side=RIGHT)

            label = Label(frame,text="Pseudo",font=('Arial 13'))
            label.pack(side=LEFT)

            validate_btn = Button(frame,text='Se connecter' ,command = self.connexion,font=("Arial", 12))
            validate_btn.place(relx=.5, rely=3,anchor= CENTER)

            self.mainloop()

        def setSetterMethodUserLogged(self,method) -> None:
            self.__methodSetUserLogged = method
