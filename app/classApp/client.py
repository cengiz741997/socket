import socket, threading, sys, pickle
from app.classApp.dataProgram import dataProgram

class client():
    def __init__(self, host: str = '127.0.0.1', port: int = 5000, nickname: str = None, buffer_recv: int = 4096):

        self.buffer_recv = buffer_recv
        self.nickname = nickname
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))

        msg_thread = threading.Thread(target=self.process_messages)
        msg_thread.daemon = True
        msg_thread.start()

        self.__sendMessageToInterface = '' # méthode

        self.data = dataProgram()

        # dès le chargement de l'application il se peut que lors de l'envoi du 1er message celui-ci obtienne une erreur liée
        # à une connexion existante a dû être fermée par l’hôte distant, de ce fait je créais un premier envoi en brut pour tester la connexion
        # entre le serveur et client-ci celle-ci échoue elle sera automatiquement rétablie et fonctionnera pour tous les autres prochains message
        self.process_send_message('')

    def process_messages(self) -> None:
        print('\t\t***** Bienvenue sur le server {}! :) *****\n'.format(self.nickname))

        # Dès qu'un utilisateur se connecte on n'envoie son pseudo au serveur qui va dispatcher sa à tous les clients connectés
        dict = {'type':self.data.MSG_USER_CONN,'client' : self.nickname}
        self.socket.send(self.parse_data(dict))

        while True:
            try:
                data = self.get_data()
                if data:
                    self.print_msg(data)
            except:
                pass

    def process_send_message(self,message: str):
        if message.lower() != 'exit':
            self.send_msg(message)
        else:
            self.close()

    def get_data(self) -> str:
        return self.socket.recv(self.buffer_recv)

    def print_msg(self, data: dict) -> None:
        self.__sendMessageToInterface(self.parse_msg(data))

    def parse_msg(self,data: str) -> tuple:
        return pickle.loads(data)

    def send_msg(self, msg: str) -> None:
        dict = {'type':self.data.MSG_USER_MESSAGE,'msg' : msg ,'client' : self.nickname}
        self.socket.send(self.parse_data(dict))

    def parse_data(self, data: dict) -> str:
        return pickle.dumps(data)

    def close(self) -> None:
        self.socket.close()

    def setMethodMessageToInterface(self, method):
        self.__sendMessageToInterface = method
